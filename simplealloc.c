#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define PAGE_SIZE 12
#define ALLOC_SIZE 1
#define MAGIC_VALUE 0xCAFEBABE
#define ITERATIONS 60
#define LOOPS 500
#define ROUNDS 50
#define MOD 2000

void set_value(int *);

int main (void)
{
int **allocs = (int **) malloc(2 * ITERATIONS * LOOPS * sizeof(int *));
int i, j = 0, k = 0, tmp = 1, carry = 1, index = 0;

printf("Buffering ...\n");

sleep(1);

while(k < ROUNDS) {
while(j < LOOPS) {

//printf("malloc: %i, i: %i, j: %i, tmp: %i, carry: %i, index: %i\n", ALLOC_SIZE << PAGE_SIZE, i, j, tmp, carry, index);
	for(i = index; i < (index + ITERATIONS); i++) {
		allocs[i] = (int *) malloc(ALLOC_SIZE << PAGE_SIZE);
		set_value(allocs[i]);
		if((i % MOD) == 0) {
			printf("Processing and displaying frame #%i.\n", (int) (i / MOD) + tmp);
			carry = (int) (i / MOD) + tmp;
		}
	}

	index = i;

	printf("Buffering ...\n");

	tmp = carry + 1;

//printf("malloc: %i, i: %i, j: %i, tmp: %i, carry: %i, index: %i\n", ALLOC_SIZE << PAGE_SIZE, i, j, tmp, carry, index);
	for(i = 0; i < (int) (ITERATIONS / 10); i++) {
		free(allocs[i]);
		if((i % MOD) == 0) {
			printf("Processing and displaying frame #%i.\n", (int) (i / MOD) + tmp);
			carry = (int) (i / MOD) + tmp;
		}
	}

//	printf("Buffering ...\n");

	tmp = carry + 1;

//printf("malloc: %i, i: %i, j: %i, tmp: %i, carry: %i, index: %i\n", ALLOC_SIZE << PAGE_SIZE, i, j, tmp, carry, index);
	for(i = 0; i < (int) (ITERATIONS / 10); i++) {
		allocs[i] = (int *) malloc(ALLOC_SIZE << PAGE_SIZE);
		set_value(allocs[i]);
		if((i % MOD) == 0) {
			printf("Processing and displaying frame #%i.\n", (int) (i / MOD) + tmp);
			carry = (int) (i / MOD) + tmp;
		}
	}

	printf("Buffering ...\n");

	tmp = carry + 1;

//printf("malloc: %i, i: %i, j: %i, tmp: %i, carry: %i, index: %i\n", ALLOC_SIZE << PAGE_SIZE, i, j, tmp, carry, index);
	for(i = index; i < (index + (int) (ITERATIONS / 2)); i++) {
		allocs[i] = (int *) malloc(ALLOC_SIZE << PAGE_SIZE);
		set_value(allocs[i]);
		if((i % MOD) == 0) {
			printf("Processing and displaying frame #%i.\n", (int) (i / MOD) + tmp);
			carry = (int) (i / MOD) + tmp;
		}
	}

//	printf("Buffering ...\n");

	index = i;
	tmp = carry + 1;

//printf("malloc: %i, i: %i, j: %i, tmp: %i, carry: %i, index: %i\n", ALLOC_SIZE << PAGE_SIZE, i, j, tmp, carry, index);
	for(i = 0; i < (int) (ITERATIONS / 4); i++) {
		free(allocs[i]);
		if((i % MOD) == 0) {
			printf("Processing and displaying frame #%i.\n", (int) (i / MOD) + tmp);
			carry = (int) (i / MOD) + tmp;
		}
	}

	printf("Buffering ...\n");

	tmp = carry + 1;

//printf("malloc: %i, i: %i, j: %i, tmp: %i, carry: %i, index: %i\n", ALLOC_SIZE << PAGE_SIZE, i, j, tmp, carry, index);
	for(i = 0; i < (int) (ITERATIONS / 4); i++) {
		allocs[i] = (int *) malloc(ALLOC_SIZE << PAGE_SIZE);
		set_value(allocs[i]);
		if((i % MOD) == 0) {
			printf("Processing and displaying frame #%i.\n", (int) (i / MOD) + tmp);
			carry = (int) (i / MOD) + tmp;
		}
	}

//	printf("Buffering ...\n");

	tmp = carry + 1;

//printf("malloc: %i, i: %i, j: %i, tmp: %i, carry: %i, index: %i\n", ALLOC_SIZE << PAGE_SIZE, i, j, tmp, carry, index);
	for(i = index; i < (index + (int) (ITERATIONS / 2)); i++) {
		allocs[i] = (int *) malloc(ALLOC_SIZE << PAGE_SIZE);
		set_value(allocs[i]);
		if((i % MOD) == 0) {
			printf("Processing and displaying frame #%i.\n", (int) (i / MOD) + tmp);
			carry = (int) (i / MOD) + tmp;
		}
	}

	printf("Buffering ...\n");

	index = i;
	tmp = carry + 1;

	j++;
}

//for(i = 0; i < (2 * ITERATIONS * LOOPS); i++) {
for(i = 0; i < index; i++) {
	if(allocs[i] != NULL)
		free(allocs[i]);
}

k++;
j = index = 0;
}

printf("Processing and displaying frame #%i.\n", tmp);
printf("Movie complete, closing media player ...\n");
sleep(3);

//for(i = 0; i < (2 * ITERATIONS * LOOPS); i++) {
for(i = 0; i < index; i++) {
	if(allocs[i] != NULL)
		free(allocs[i]);
}

printf("\nHope you enjoyed the movie!\n");
sleep(3);
}

void set_value (int *target)
{
	int j;

	for(j = 0; j < ((ALLOC_SIZE << PAGE_SIZE) / 4); j += 4)
		*(target + j) = MAGIC_VALUE;
}
