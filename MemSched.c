//device driver linux mmap

#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/pagemap.h>
#include <linux/rmap.h>
#include <linux/list.h>
#include <linux/types.h>
#include <linux/wait.h>
#include "bitmap.h"

//user addr to kernel addr page mapping per process
#define CVECT_ALLOC() memset((void *) __get_free_page(GFP_KERNEL), 0, PAGE_SIZE)
#define CVECT_FREE(x) free_page((unsigned long) x)
#include "cvect.h"

#define MAJOR_NUMBER 250	/*unused major number for static assignment*/
#define DEVICE_NAME "memsched"
#define DEBUG 0
#define TEST_FILL 0xDEADBEEF
#define META_DATA_ORDER 3
#define INIT_POOL_SIZE 128000
#define POOL_SCALE_FACTOR 1
#define PAGE_NUM(addr) ((unsigned long) addr >> 12)
#define PAGE_ADDR(num) ((unsigned long) num << 12)

//file operations
int ms_mmap (struct file *, struct vm_area_struct *);
int ms_open (struct inode *, struct file *);
int ms_release (struct inode *, struct file *);

struct file_operations memsched_fops = {
	.mmap =	ms_mmap,
	.open =	ms_open,
	.release = ms_release,
	.owner = THIS_MODULE,
};

//virtual memory area operations
struct page *ms_vmnopage (struct vm_area_struct*, unsigned long, int*);
void ms_vmclose (struct vm_area_struct*);

struct vm_operations_struct memsched_vmops = {
	.nopage = ms_vmnopage,
	.close = ms_vmclose,
};

//memory pool manipulation
void get_pool_pg(unsigned long *addr);
int add_pool_pg(unsigned long vaddr);
int grow_pool(void);
void destroy_pool(void);
void print_pool(void);

//processes manipulation
struct proc_desc *find_proc(int pid);
int add_proc(struct file *filp);
void rem_proc(int pid);
void destroy_procs(void);
void cannibalize(int pid);
void set_relinquish(void);
void clr_relinquish(void);
void print_procs(void);

//meta data structures
struct bits {
	int used:1, mapped:1;
} __attribute__((packed));

struct page_data {
	void *start, *end;
	int relinquish;
	int got_meta;
	uint32_t bits[0];
};

struct proc_desc {
	struct list_head procs;
	struct task_struct *task;
	int watermark_pgcount;
	int current_pgcount;
	struct page_data *page_usage;
	cvect_t *utov;
	int slept;
};

struct page_list {
	struct list_head first_pg;
	void *kvirt;
};

struct mem_pool {
	int high_watermark;
	int current_pgcount;
	struct page_list pages;
};

static struct mem_pool page_pool;
static struct proc_desc processes;
static int victim_pid;
static int threshold;
static int num_active;
//static wait_queue_head_t wque;
static DECLARE_WAIT_QUEUE_HEAD(wque);
static DEFINE_MUTEX(devmut);

int memsched_init (void)
{
	static int major;
	int i;
	struct page_list *tmp_pg;

	major = register_chrdev(MAJOR_NUMBER, DEVICE_NAME, &memsched_fops);

	if (major < 0) {
		printk(KERN_ALERT "ms_in: error assigning major number\n");
		return major;
	}

	page_pool.high_watermark = INIT_POOL_SIZE;
	page_pool.current_pgcount = 0;
	INIT_LIST_HEAD(&page_pool.pages.first_pg);
	for(i = 0; i < INIT_POOL_SIZE; i++) {
		tmp_pg = (struct page_list *) __get_free_page(GFP_KERNEL);

		if(!tmp_pg)
			goto pool_inc_failed;

		tmp_pg->kvirt = (void *) tmp_pg;
//		printk(KERN_ALERT "ms_in: add page %x to free list.\n", (unsigned long) tmp_pg->kvirt);
		list_add(&tmp_pg->first_pg, &page_pool.pages.first_pg);
		page_pool.current_pgcount++;
	}

	//init_waitqueue_head(&wque);
	INIT_LIST_HEAD(&processes.procs);
	victim_pid = 0;
	num_active = 0;
	threshold = 1000;

	printk(KERN_ALERT "ms_in: registered with major #250\n");
	return major;

pool_inc_failed:
	printk(KERN_ALERT "ms_in: problem in module init, pool pages could not be added.\n");
	return -1;
}

void memsched_cleanup (void)
{
	printk(KERN_ALERT "ms_cu: cleaning up processes.\n");
	destroy_procs();

	printk(KERN_ALERT "ms_cu: cleaning up memory pool.\n");
	destroy_pool();

	unregister_chrdev(MAJOR_NUMBER, DEVICE_NAME);
	printk(KERN_ALERT "ms_cu: unregistered.\n");
}

int ms_mmap (struct file *filp, struct vm_area_struct *vma)
{
	unsigned long pg_addr, vma_addr;
	struct page *pg = NULL;
	struct proc_desc *proc;
	int i;
/*//******************************************************************
	wait_queue_t *e;
//*******************************************************************/

//print_procs();

	while(1) {
		mutex_lock(&devmut);

		proc = find_proc(current->pid);

		//printk(KERN_ALERT "ms_mm: device mmap called, process pid %i and pool has %i pages.\n", proc->task->pid, page_pool.current_pgcount);

		if(!proc)
			goto get_proc_failed;

		if(proc->page_usage->got_meta == 0) {

			if(num_active < 2) {
				num_active++;
			}
			else {
//				proc->slept = 0;
//				printk(KERN_ALERT "ms_mm: process pid %i sleeping on meta mapping\n", current->pid);
				mutex_unlock(&devmut);

/*//******************************************************************
e = (wait_queue_t *) kmalloc(sizeof(wait_queue_t), GFP_KERNEL);
init_waitqueue_entry(e, current);
add_wait_queue(&wque, e);
//*******************************************************************/

				wait_event_interruptible(wque, num_active < 2);
				mutex_lock(&devmut);

proc->slept = 1;

//				printk(KERN_ALERT "ms_mm: process pid %i was woken from sleep on meta\n", current->pid);
				num_active++;
			}

			printk(KERN_ALERT "ms_mm: mapping meta data to process pid %i.\n", proc->task->pid);
			for(vma_addr = vma->vm_start, i = 0; (vma_addr < vma->vm_end) && (i < (1 << META_DATA_ORDER)); vma_addr += PAGE_SIZE, i++) {

				pg = virt_to_page(((char *)proc->page_usage) + i * 4096);

				if(!pg)
					goto get_page_failed;

				get_page(pg);

				if(vm_insert_page(vma, vma_addr, pg))
					goto insert_failed;

				cvect_add(proc->utov, (void *) ((char *)proc->page_usage + i * 4096), (long) PAGE_NUM(vma_addr));

//				printk(KERN_ALERT "ms_mm: meta data to upage #%i\n", (int) PAGE_NUM(vma_addr));
				bitmap_set(proc->page_usage->bits, PAGE_NUM(vma_addr) * 2 + 1);
				bitmap_set(proc->page_usage->bits, PAGE_NUM(vma_addr) * 2);
				proc->current_pgcount++;
			} //endfor

			proc->page_usage->start = vma->vm_start;
			proc->page_usage->end = vma->vm_end;

			goto fin_mem_map;
		} //endif got_meta
		else {
			if((num_active >= 2) && (proc->current_pgcount < threshold) && (proc->slept == 0))
			{
//				printk(KERN_ALERT "ms_mm: process pid %i waking a process and sleeping on memory gathering.\n", proc->task->pid);
				num_active--;
				wake_up_interruptible(&wque);
				mutex_unlock(&devmut);

/*//******************************************************************
e = (wait_queue_t *) kmalloc(sizeof(wait_queue_t), GFP_KERNEL);
init_waitqueue_entry(e, current);
add_wait_queue(&wque, e);
//*******************************************************************/

				wait_event_interruptible(wque, num_active < 2);
				mutex_lock(&devmut);

proc->slept = 1;

				num_active++;
//				printk(KERN_ALERT "ms_mm: process pid %i was woken from sleep on memory\n", current->pid);
				mutex_unlock(&devmut);
				continue;
			}

			if(((int) (vma->vm_end - vma->vm_start) / 4096) > page_pool.current_pgcount) {
				printk(KERN_ALERT "ms_mm: vm start: %x, vm end: %x, pool pg count: %i TAG\n", vma->vm_start, vma->vm_end, page_pool.current_pgcount);
				if(waitqueue_active(&wque)) {
					//printk(KERN_ALERT "ms_mm: process page count: %i\n", proc->current_pgcount);
					//printk(KERN_ALERT "ms_mm: pool small, queue active TAG\n.");
					mutex_unlock(&devmut);
					wait_event_interruptible(wque, ((vma->vm_end - vma->vm_start) / 4096) > page_pool.current_pgcount);
					continue;
				} //endif wque not empty
				else {
					//printk(KERN_ALERT "ms_mm: pool small, queue inactive TAG\n.");
					//cannibalize(current->pid);
					//wake_up_interruptible(&wque);
					mutex_unlock(&devmut);
					continue;
				} //endif wque empty
			} //endif pool too small to satisfy request
			else {
				proc->page_usage->relinquish = 1;
				for(vma_addr = vma->vm_start; vma_addr < vma->vm_end; vma_addr += PAGE_SIZE) {
					printk(KERN_ALERT "ms_mm: providing new memory to process pid %i.\n", proc->task->pid);

					get_pool_pg(&pg_addr);

					if(!pg_addr)
						goto get_mem_failed;

					pg = virt_to_page(pg_addr);

					if(!pg)
						goto get_page_failed;

					if(vm_insert_page(vma, vma_addr, pg))
						goto insert_failed;

					cvect_add(proc->utov, (void *) pg_addr, (long) PAGE_NUM(vma_addr));

					bitmap_set(proc->page_usage->bits, PAGE_NUM(vma_addr) * 2 + 1);
					proc->current_pgcount++;
				} //endfor each page to be mapped in

				if(vma->vm_start < proc->page_usage->start)
					proc->page_usage->start = vma->vm_start;
				else if(vma->vm_end > proc->page_usage->end)
					proc->page_usage->end = vma->vm_end;

				proc->slept = 0;

				goto fin_mem_map;
			} //endif pool large enough for request
		} //endif mapping new memory to proc
	} //close while

fin_mem_map:
	vma->vm_flags |= (VM_RESERVED | VM_INSERTPAGE);
	vma->vm_ops = &memsched_vmops;

	//printk(KERN_ALERT "ms_mm: device mmap succeeded.\n");
	mutex_unlock(&devmut);
	return 0;

get_proc_failed:
	printk(KERN_ALERT "ms_mm: mmap failed, failed to locate the proc meta data.\n");
	mutex_unlock(&devmut);
	return -EAGAIN;

//pool_inc_failed:
//	printk(KERN_ALERT "Memscheduler: mmap failed, expansion of the memory pool failed because a page could not be obtained.\n");
//	return -ENOMEM;

get_mem_failed:
	printk(KERN_ALERT "ms_mm: mmap failed, could not allocate kernel page.\n");
	mutex_unlock(&devmut);
	return -ENOMEM;

get_page_failed:
	printk(KERN_ALERT "ms_mm: mmap failed, virt_to_page failed.\n");
	free_page(pg_addr);
	mutex_unlock(&devmut);
	return -EAGAIN;

insert_failed:
	printk(KERN_ALERT "ms_mm: mmap failed, page insert into VMA failed.\n");
	free_page(pg_addr);
	mutex_unlock(&devmut);
	return -EAGAIN;
}

int ms_open (struct inode *i_node, struct file *filp)
{
mutex_lock(&devmut);

//print_pool();

	if(add_proc(filp))
		goto add_proc_failed;

	printk(KERN_ALERT "ms_op: device opened.\n");

mutex_unlock(&devmut);

	return 0;

add_proc_failed:
	printk(KERN_ALERT "ms_od: device open failed, proc meta data creation / storage failed.\n");
	mutex_unlock(&devmut);
	return -EAGAIN;
}

int ms_release (struct inode *i_node, struct file *filp)
{
mutex_lock(&devmut);
	num_active--;
	wake_up_interruptible(&wque);
	printk(KERN_ALERT "ms_rd: device released.\n");
mutex_unlock(&devmut);

	return 0;
}

struct page *ms_vmnopage (struct vm_area_struct *vma, unsigned long address, int* type)
{
	struct page *pageptr;
	unsigned long physaddr = address - vma->vm_start + vma->vm_pgoff;

	printk(KERN_ALERT "ms_vn: nopage op called.\n");

	pageptr = virt_to_page(__va(physaddr));

	if(pageptr == NULL) {
		printk(KERN_ALERT "ms_vn: nopage failed, could not get struct page.\n");
		return NOPAGE_OOM;
	}

	get_page(pageptr);
	return pageptr;
}

void ms_vmclose (struct vm_area_struct *vma)
{
	struct proc_desc *tmp_pd;
	unsigned long kaddr, uaddr;
	int ppg_count, mpg_count;

mutex_lock(&devmut);
	if(victim_pid == 0)
		tmp_pd = find_proc(current->pid);
	else
		tmp_pd = find_proc(victim_pid);

	if(!tmp_pd)
		goto vm_close_failed;

	ppg_count = tmp_pd->current_pgcount;
	mpg_count = page_pool.current_pgcount;

	for(uaddr = vma->vm_start; uaddr < vma->vm_end; uaddr += 4096) {
		bitmap_unset(tmp_pd->page_usage->bits, PAGE_NUM(uaddr) * 2);
		bitmap_unset(tmp_pd->page_usage->bits, PAGE_NUM(uaddr) * 2 + 1);
		kaddr = (unsigned long) cvect_lookup(tmp_pd->utov, (long) PAGE_NUM(uaddr));
		cvect_del(tmp_pd->utov, (long) PAGE_NUM(uaddr));
		add_pool_pg(kaddr);
		tmp_pd->current_pgcount--;
	}

	printk(KERN_ALERT "ms_vc: Proc pid #%i start: %i, end: %i pages. Memory pool start: %i, end: %i pages.\n", tmp_pd->task->pid, ppg_count, tmp_pd->current_pgcount, mpg_count, page_pool.current_pgcount);

	tmp_pd->slept = 0;

mutex_unlock(&devmut);

	return;

vm_close_failed:
	printk(KERN_ALERT "ms_vc: close failed, proc pid #%i or %i could not be located.\n", victim_pid, current->pid);
	mutex_unlock(&devmut);
	return;
}

void get_pool_pg(unsigned long *addr) {
	struct page_list *tmp_pg;

	tmp_pg = list_entry((&page_pool.pages.first_pg)->next, struct page_list, first_pg);

	list_del((&page_pool.pages.first_pg)->next);
	page_pool.current_pgcount--;

	if(!tmp_pg)
		goto get_pool_pg_failed;

//	printk(KERN_ALERT "ms_gp: grabbing page w/ kvirt %x\n", (unsigned long) tmp_pg->kvirt);

	*addr = (unsigned long) tmp_pg->kvirt;

	return;

get_pool_pg_failed:
	printk(KERN_ALERT "ms_gp: getting a page from the memory pool failed.\n");
}

int add_pool_pg(unsigned long vaddr) {
	struct page_list *tmp_pg = (struct page_list *) vaddr;

//	printk(KERN_ALERT "ms_ap: add_pool_pg w/ vaddr %x\n", vaddr);

	if(!tmp_pg)
		goto pool_inc_failed;

	tmp_pg->kvirt = (void *) tmp_pg;

//	printk(KERN_ALERT "ms_ap: add page vaddr %x as kvirt %x.\n", vaddr, (unsigned long) tmp_pg->kvirt);

	list_add(&tmp_pg->first_pg, &page_pool.pages.first_pg);
	page_pool.current_pgcount++;

	return 0;

pool_inc_failed:
	printk(KERN_ALERT "ms_ap: add page to pool failed, could not obtain a page.\n");
	return -ENOMEM;
}

void destroy_pool(void)
{
	struct list_head *cursor, *tmp_store;
	struct page_list *tmp_pl;

	list_for_each_safe(cursor, tmp_store, &page_pool.pages.first_pg) {
		tmp_pl = list_entry(cursor, struct page_list, first_pg);
		//printk(KERN_ALERT "Memscheduler: Freeing page starting at %lu\n", (unsigned long) tmp_pl->kvirt));
		free_page((unsigned long) tmp_pl->kvirt);
		list_del(cursor);
	}
}

void print_pool(void)
{
	struct list_head *cursor, *tmp_store;
	struct page_list *tmp_pl;

	printk(KERN_ALERT "ms_ppo: *********************************************************************.\n");

	list_for_each_safe(cursor, tmp_store, &page_pool.pages.first_pg) {
		tmp_pl = list_entry(cursor, struct page_list, first_pg);
		printk(KERN_ALERT "ms_ppo: kvirt %x.\n", (unsigned long) tmp_pl->kvirt);
	}

	printk(KERN_ALERT "ms_ppo: done printing pool.\n");
}

struct proc_desc *find_proc(int pid)
{
	struct list_head *cursor;
	struct proc_desc *tmp_pd;

	list_for_each(cursor, &processes.procs) {
		tmp_pd = list_entry(cursor, struct proc_desc, procs);

		if(tmp_pd->task->pid == pid)
			return tmp_pd;
	}

	return NULL;
}

int add_proc(struct file *filp)
{
	struct proc_desc *tmp_procd = (struct proc_desc *) kmalloc(sizeof(struct proc_desc), GFP_KERNEL);
	struct page_data *tmp_paged = (struct page_data *) __get_free_pages(GFP_KERNEL, META_DATA_ORDER);
	cvect_t *tmp_vect = cvect_alloc();

	if(!tmp_procd || !tmp_paged || !tmp_vect)
		goto struct_alloc_failed;

	memset((void *) tmp_paged, 0, PAGE_SIZE * (1 << META_DATA_ORDER));

	tmp_paged->start = (void *) 0;
	tmp_paged->end = (void *) 1;
	tmp_paged->relinquish = 0;
	tmp_paged->got_meta = 0;
	filp->private_data = tmp_paged;

	cvect_init(tmp_vect);

	tmp_procd->task = current;
	tmp_procd->watermark_pgcount = 128000;
	tmp_procd->current_pgcount = 0;
	tmp_procd->page_usage = tmp_paged;
	tmp_procd->utov = tmp_vect;
	tmp_procd->slept = 1;

	list_add(&tmp_procd->procs, &processes.procs);

	printk(KERN_ALERT "ms_apr: proc pid #%i registered with the device.\n", tmp_procd->task->pid);

	return 0;

struct_alloc_failed:
	printk(KERN_ALERT "ms_apr: allocation of bookkeeping structures failed.\n");
	kfree(tmp_procd);
	free_page((unsigned long) tmp_paged);
	return -EAGAIN;
}

void rem_proc(int pid)
{
	struct list_head *cursor, *tmp_store;
	struct proc_desc *tmp_pd;

	printk(KERN_ALERT "ms_rpr: removing process pid %i.\n", pid);

	list_for_each_safe(cursor, tmp_store, &processes.procs) {
		tmp_pd = list_entry(cursor, struct proc_desc, procs);
		if(tmp_pd->task->pid == pid) {
			list_del(cursor);
			kfree(tmp_pd);
			return;
		}
	}
}

void destroy_procs(void)
{
	struct list_head *cursor, *tmp_store;
	struct proc_desc *tmp_pd;

	list_for_each_safe(cursor, tmp_store, &processes.procs) {
		tmp_pd = list_entry(cursor, struct proc_desc, procs);
		//printk(KERN_ALERT "Memscheduler: In cleanup freeing process with PID %i\n", tmp_pd->pid);
		cvect_free(tmp_pd->utov);
		free_pages((unsigned long) tmp_pd->page_usage, META_DATA_ORDER);
		list_del(cursor);
		kfree(tmp_pd);
	}
}

void cannibalize(int pid)
{
	struct list_head *cursor;
	struct proc_desc *tmp_pd;
	struct mm_struct *p_mm;
	int i, ret, opc;

	//printk(KERN_ALERT "ms_cp: proc pid #%i cannibalizing. Mem pool start: %i pages.\n", pid, opc = page_pool.current_pgcount);
	//print_procs();

	list_for_each(cursor, &processes.procs) {
		tmp_pd = list_entry(cursor, struct proc_desc, procs);

		if(tmp_pd->task->pid == pid)
			continue;
		//if(tmp_pd->task->pid != pid)
		//{

		victim_pid = tmp_pd->task->pid;

		//printk(KERN_ALERT "ms_cp: cannabalizing proc pid #%i\n", tmp_pd->task->pid);
		p_mm = tmp_pd->task->mm;

		for(i = (int) PAGE_NUM(tmp_pd->page_usage->start); i <= (int) PAGE_NUM(tmp_pd->page_usage->end); i++) {
			//printk(KERN_ALERT "ms_cp: pid #%i, page #%i - mapped: %i, used: %i\n", tmp_pd->task->pid, i, bitmap_check(tmp_pd->page_usage->bits, i * 2 + 1), bitmap_check(tmp_pd->page_usage->bits, i * 2));
			if(bitmap_check(tmp_pd->page_usage->bits, i * 2 + 1) && !bitmap_check(tmp_pd->page_usage->bits, i * 2)) {
				down_write(&p_mm->mmap_sem);
mutex_unlock(&devmut);
				ret = do_munmap(p_mm, (unsigned long) i << 12, (size_t) 4096);
mutex_lock(&devmut);
	    		up_write(&p_mm->mmap_sem);

				if(ret)
					printk(KERN_ALERT "ms_cp: failed to cannibalize page, do_munmap failed.\n");
			}
		} //endfor
		//} //endif
	} //endlistforeach

	victim_pid = 0;

	printk(KERN_ALERT "ms_cp: cannabalization complete. Mem pool end: %i, recovered: %i pages.\n", page_pool.current_pgcount, page_pool.current_pgcount - opc);
}

void set_relinquish(void)
{
	struct list_head *cursor, *tmp_store;
	//struct proc_desc *tmp_pd;

	list_for_each_safe(cursor, tmp_store, &processes.procs) {
		list_entry(cursor, struct proc_desc, procs)->page_usage->relinquish = 1;
	}
}

void clr_relinquish(void)
{
	struct list_head *cursor, *tmp_store;
	//struct proc_desc *tmp_pd;

	list_for_each_safe(cursor, tmp_store, &processes.procs) {
		list_entry(cursor, struct proc_desc, procs)->page_usage->relinquish = 0;
	}
}

void print_procs(void)
{
	struct list_head *cursor, *tmp_store;
	struct proc_desc *tmp_pd;

	list_for_each_safe(cursor, tmp_store, &processes.procs) {
		tmp_pd = list_entry(cursor, struct proc_desc, procs);
		printk(KERN_ALERT "ms_pp: Printing proc pid %i. %i current pages, %i is watermark.\n", tmp_pd->task->pid, tmp_pd->current_pgcount, tmp_pd->watermark_pgcount);
		//cvect_free(tmp_pd->utov);
		//free_pages((unsigned long) tmp_pd->page_usage, META_DATA_ORDER);
		//list_del(cursor);
		//kfree(tmp_pd);
	}
}

module_init(memsched_init);
module_exit(memsched_cleanup);
