obj-m	+= MemSched.o
KDIR	:= /usr/src/linux-2.6.18.8
PWD	:= $(shell pwd)

default:
	$(MAKE) -C $(KDIR) SUBDIRS=$(PWD) modules
